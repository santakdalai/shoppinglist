package routes

import "github.com/gin-gonic/gin"

// ShoppingListGroup creates all the routes related to Shopping List
func ShoppingListGroup(r *gin.Engine) {
	shoppingListGroup := r.Group("/shopping-list")
	{
		shoppingListGroup.POST("create", shoppingListController.CreateShoppingList)
		shoppingListGroup.POST("share", shoppingListController.ShareShoppingList)
		shoppingListGroup.POST("delete", shoppingListController.DeleteShoppingList)
		shoppingListGroup.POST("add-item", shoppingListController.AddItem)
		shoppingListGroup.GET("items", shoppingListController.GetItems)
	}
}
