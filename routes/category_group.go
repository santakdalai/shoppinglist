package routes

import "github.com/gin-gonic/gin"

// CategoryGroup creates all the routes related to Category
func CategoryGroup(r *gin.Engine) {
	categoryGroup := r.Group("/category")
	{
		categoryGroup.POST("create", categoryController.CreateCategory)
	}
}
