package routes

import (
	"shoppinglist/controller"

	"github.com/gin-gonic/gin"
)

var (
	signupController       = new(controller.SignupController)
	shoppingListController = new(controller.ShoppingListController)
	userController         = new(controller.UserController)
	categoryController     = new(controller.CategoryController)
	itemController         = new(controller.ItemController)
)

// InitRoutes initializes the required routes for the application
func InitRoutes(r *gin.Engine) {
	UserGroup(r)
	ShoppingListGroup(r)
	CategoryGroup(r)
	ItemGroup(r)
}
