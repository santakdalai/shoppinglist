package routes

import "github.com/gin-gonic/gin"

// ItemGroup creates all the routes related to Items
func ItemGroup(r *gin.Engine) {
	itemGroup := r.Group("/item")
	{
		itemGroup.POST("mark", itemController.MarkItemBought)
		itemGroup.POST("delete", itemController.DeleteItem)
	}
}
