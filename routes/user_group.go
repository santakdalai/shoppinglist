package routes

import "github.com/gin-gonic/gin"

// UserGroup creates all the routes related to User
func UserGroup(r *gin.Engine) {
	userGroup := r.Group("/user")
	{
		userGroup.POST("signup", signupController.AddUser)
		userGroup.POST("login", signupController.Login)
		userGroup.POST("logout", signupController.Logout)
		userGroup.GET("fetch-shopping-list", userController.FetchShoppingLists)
		userGroup.GET("get-all-user-ids", userController.GetAllUsers)
	}
}
