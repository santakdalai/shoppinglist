# Go parameters
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOMOD=$(GOCMD) mod
BINARY_NAME=shopping_list
GOLINT=golint

.PHONY: test

all: test clean build

## lint: Runs the linter on the source
lint:
	$(GOLINT) -set_exit_status ./...

## build: Gathers the dependencies and builds the binary
build: lint
	$(GOMOD) tidy
	$(GOBUILD) -o $(BINARY_NAME) -v

## test: Runs all available tests in the source code
test:
	$(GOTEST) -v ./...

## clean: Clean build files. Runs `go clean` internally
clean: 
	$(GOCLEAN)
	rm -f $(BINARY_NAME)

## run: Builds the binary and starts the execution
run: build
	./$(BINARY_NAME)

help: Makefile
	@echo
	@echo "Available commands:"
	@echo
	@sed -n 's/^##//p' $< | column -t -s ':' |  sed -e 's/^/ /'
	@echo