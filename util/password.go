package util

import (
	gopassword "github.com/sethvargo/go-password/password"
	"golang.org/x/crypto/bcrypt"
)

// HashPassword Hhashes a given password string using bcrypt
func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return string(bytes), err
}

// CheckPasswordHash matches a given password and a stored hash
func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

// GenerateRandomPassword generates a random password
func GenerateRandomPassword() (string, error) {
	return gopassword.Generate(10, 2, 2, false, false)
}
