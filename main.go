package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"shoppinglist/config"
	"shoppinglist/controller"
	"shoppinglist/model"
	"shoppinglist/routes"
	"strconv"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/spf13/viper"

	"github.com/gin-contrib/logger"
	"github.com/gin-gonic/gin"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

var db *gorm.DB

func init() {
	config.LoadConfig()

	// initialise DB
	initDB()

}

func initDB() {
	//open a db connection

	var err error
	var dbConfig = struct {
		username             string
		password             string
		dbname               string
		ip                   string
		port                 int
		maxConnectionAttempt int
	}{
		username:             viper.GetString("database.username"),
		password:             viper.GetString("database.password"),
		dbname:               viper.GetString("database.dbname"),
		ip:                   viper.GetString("database.ip"),
		port:                 viper.GetInt("database.port"),
		maxConnectionAttempt: viper.GetInt("database.max_connection_attempt"),
	}
	dbConnStr := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
		dbConfig.username, dbConfig.password, dbConfig.ip, strconv.Itoa(dbConfig.port), dbConfig.dbname)

	var attemptCount int
	for attemptCount = 0; attemptCount < dbConfig.maxConnectionAttempt; attemptCount++ {
		db, err = gorm.Open("mysql", dbConnStr)
		if err == nil {
			break
		}
		fmt.Printf("Attempting to connect to database. Attempt no. %d\n", attemptCount)
		time.Sleep(5 * time.Second)
	}

	if attemptCount == dbConfig.maxConnectionAttempt {
		panic("failed to connect database" + err.Error())
	}

	//Migrate the schema
	db.AutoMigrate(&model.User{}, &model.ShoppingList{}, &model.Item{}, &model.Category{})
}

func main() {

	zerolog.SetGlobalLevel(zerolog.Level(viper.GetInt("logging.level")))
	logFile, err := os.OpenFile(viper.GetString("logging.path"), os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal().Msgf("Error creating or writing to the log file. %s", err.Error())
	}
	defer logFile.Close()

	log.Logger = log.Output(io.MultiWriter(logFile,
		zerolog.ConsoleWriter{
			Out:     os.Stderr,
			NoColor: false,
		},
	))

	controller.InitializeController(db)

	router := gin.New()

	// Add the logger middleware
	router.Use(logger.SetLogger())

	router.GET("/ping", func(c *gin.Context) {
		log.Info().Msg("Received ping message")
		c.JSON(http.StatusOK, gin.H{
			"message": "pong",
		})
	})

	routes.InitRoutes(router)

	log.Info().Str("Port", strconv.Itoa(viper.GetInt("webserver.port"))).Msg("Starting web server")
	router.Run(fmt.Sprintf(":%s", strconv.Itoa(viper.GetInt("webserver.port"))))

}
