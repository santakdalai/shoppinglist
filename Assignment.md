The assignment is to build a simple Shopping List app, in which multiple users can create a shopping list, either for themselves or in conjunction with another user. In this list we can create items that need to be acquired.

We have defined multiple stories below, but you are always free to add new stories should you find that helpful/necessary.

The project should be developed in Mendix for the front-end, that consumes a backend REST API developed in Golang. The Mendix development user can be created using for instance a Yandex email https://mail.yandex.com/.

You will need to register using your email at https://signup.mendix.com/ and go through part or whole of the following courses:

https://gettingstarted.mendixcloud.com/link/path/82

https://docs.mendix.com/howto/integration/

The project should be developed using the best practices in mind (tests, documentation, etc). Also, the project should be hosted using Git, so we can follow the commit history.

The user stories to be implemented are the following:

* As a user I want to be able to login to the application.
* As a user I want to be able to create a shopping list (with information about title, creation time and user).
* As a user I want to be able to add items to the shopping list, assigning it a specific category.
* As I user I want to be able to mark items as bought, keeping information regarding who did it and when.
* As a user I want to share the shopping list with another user, that can also modify it.

If you come up with undefined requirements, feel free to write stories for that. This is what we do in our daily work and these stories will be used to evaluate your solution. We’ve deliberately left the specific implementation details out - we want to give you space to be creative with your approach to the problem, and impress us with your skills and experience.