FROM golang:1.13.6-alpine3.11
RUN apk add --no-cache bash coreutils make git
RUN go get -u golang.org/x/lint/golint

RUN mkdir -p /go/src/workspace/shoppinglist/log

WORKDIR /go/src/workspace/shoppinglist
COPY . .
RUN make build

EXPOSE 9797

ENTRYPOINT ["./shopping_list"]
