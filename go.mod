module shoppinglist

go 1.13

require (
	github.com/DATA-DOG/go-sqlmock v1.4.1
	github.com/gin-contrib/logger v0.0.2
	github.com/gin-gonic/gin v1.6.2
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/jinzhu/gorm v1.9.12
	github.com/rs/zerolog v1.18.0
	github.com/satori/go.uuid v1.2.0
	github.com/sethvargo/go-password v0.1.3
	github.com/spf13/viper v1.6.2
	github.com/stretchr/testify v1.4.0
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd
)
