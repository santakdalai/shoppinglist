package model

import (
	"github.com/jinzhu/gorm"
)

// Item model - to represent an item in the shopping list
type Item struct {
	gorm.Model
	Name           string `gorm:"not null"`
	Description    string
	ShoppingListID uint
	CategoryID     uint
	IsBought       bool
	BoughtBy       string `gorm:"foreignkey:BoughtBy;association_foreignkey:UserID"`
}
