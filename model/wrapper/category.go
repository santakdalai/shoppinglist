package wrapper

import (
	"shoppinglist/model"

	"github.com/jinzhu/gorm"
)

// CategoryWrapper is wrapper class for model Category. It helps in easy dependency injection
type CategoryWrapper struct {
	DB *gorm.DB
}

// Insert inserts a Category to the database
func (c *CategoryWrapper) Insert(data interface{}) error {
	category := data.(*model.Category)

	return c.DB.Create(&category).Error
}

// Update updates a Category in the database
func (c *CategoryWrapper) Update(data interface{}) error {
	category := data.(model.Category)

	return c.DB.Save(&category).Error
}

// CreateCategoryWrapper created the CategoryWrapper using a given reference to gorm.DB
func CreateCategoryWrapper(db *gorm.DB) *CategoryWrapper {
	return &CategoryWrapper{
		DB: db,
	}
}

// GetCategory searches a category in the database based on the category id and returns it
func (c *CategoryWrapper) GetCategory(categoryID uint) (model.Category, error) {
	var category model.Category

	err := c.DB.First(&category, categoryID).Error
	if err != nil {
		return model.Category{}, err
	}

	return category, nil
}

// AddItem adds an Item to a given category and returns the modified category
func (c *CategoryWrapper) AddItem(item model.Item) (model.Category, error) {
	category, err := c.GetCategory(item.CategoryID)

	if err != nil {
		return model.Category{}, err
	}

	category.Items = append(category.Items, item)
	err = c.DB.Save(&category).Error
	if err != nil {
		return model.Category{}, err
	}

	return category, nil
}
