package wrapper

import (
	"github.com/jinzhu/gorm"
	"shoppinglist/model"
)

// UserWrapper is wrapper class for model User. It helps in easy dependency injection
type UserWrapper struct {
	DB *gorm.DB
}

// Insert inserts a User to the database
func (u *UserWrapper) Insert(data interface{}) error {
	user := data.(*model.User)

	return u.DB.Create(&user).Error
}

// Update updates a User in the database
func (u *UserWrapper) Update(data interface{}) error {
	user := data.(model.User)

	return u.DB.Save(&user).Error
}

// CreateUserWrapper creates the UserWrapper using a given reference to gorm.DB
func CreateUserWrapper(db *gorm.DB) *UserWrapper {
	return &UserWrapper{
		DB: db,
	}
}

// GetUser searches a User in the database based on the userID and returns it
func (u *UserWrapper) GetUser(userID string) (model.User, error) {
	var user model.User

	err := u.DB.Where(&model.User{UserID: userID}).First(&user).Error
	if err != nil {
		return model.User{}, err
	}

	return user, nil
}

// GetAllUsers fetches list of all available user IDs
func (u *UserWrapper) GetAllUsers() []string {
	var users []model.User
	userIDs := make([]string,0)

	err := u.DB.Find(&users).Error
	if err != nil {
		return userIDs
	}

	for _,user := range users {
		userIDs = append(userIDs, user.UserID)
	}
	return userIDs
}

// UserAlreadyExists checks if a user ID is already present in the database
func (u *UserWrapper) UserAlreadyExists(userID string) bool {
	var user model.User

	return !gorm.IsRecordNotFoundError(u.DB.Where(&model.User{UserID: userID}).First(&user).Error)
}

// GetShoppingLists fetches the shopping lists of a given user
func (u *UserWrapper) GetShoppingLists(userID string) []model.ShoppingList {
	user, err := u.GetUser(userID)
	if err != nil {
		return nil
	}
	u.DB.Preload("ShoppingLists").Find(&user)
	for i := 0; i < len(user.ShoppingLists); i++ {
		u.DB.Preload("Users").Find(&user.ShoppingLists[i])
		u.DB.Preload("Items").Find(&user.ShoppingLists[i])
	}
	return user.ShoppingLists
}
