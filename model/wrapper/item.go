package wrapper

import (
	"github.com/jinzhu/gorm"
	"shoppinglist/model"
)

// ItemWrapper is wrapper class for model Item. It helps in easy dependency injection.
type ItemWrapper struct {
	DB *gorm.DB
}

// Insert inserts a Category to the database
func (i *ItemWrapper) Insert(data interface{}) error {
	item := data.(*model.Item)

	return i.DB.Create(&item).Error
}

// Update updates a Category in the database
func (i *ItemWrapper) Update(data interface{}) error {
	item := data.(*model.Item)

	return i.DB.Save(&item).Error
}

// GetItem searches an Item in the database based on the ID and returns it
func (i *ItemWrapper) GetItem(itemID uint) (model.Item, error) {
	var item model.Item

	err := i.DB.First(&item, itemID).Error
	if err != nil {
		return model.Item{}, err
	}

	return item, nil
}

// CreateItemWrapper creates the ItemWrapper using a given reference to gorm.DB
func CreateItemWrapper(db *gorm.DB) *ItemWrapper {
	return &ItemWrapper{
		DB: db,
	}
}

// MarkBought marks an item a bought
func (i *ItemWrapper) MarkBought(itemID uint, userID string) (model.Item, error) {
	var item model.Item

	err := i.DB.First(&item, itemID).Error
	if err != nil {
		return model.Item{}, err
	}

	user, err := CreateUserWrapper(i.DB).GetUser(userID)
	if err != nil {
		return model.Item{}, err
	}

	item.IsBought = true
	item.BoughtBy = user.UserID

	err = i.DB.Save(&item).Error
	if err != nil {
		return model.Item{}, err
	}

	return item, nil
}

// Delete deletes an item in the database
func (i *ItemWrapper) Delete(data interface{}) error {
	item := data.(model.Item)

	return i.DB.Delete(&item).Error
}
