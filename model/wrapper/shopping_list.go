package wrapper

import (
	"shoppinglist/model"

	"github.com/jinzhu/gorm"
)

// ShoppingListWrapper is wrapper class for model ShoppingList. It helps in easy dependency injection
type ShoppingListWrapper struct {
	DB *gorm.DB
}

// Insert inserts a ShoppingList to the database
func (s *ShoppingListWrapper) Insert(data interface{}) error {
	shoppingList := data.(*model.ShoppingList)

	return s.DB.Create(&shoppingList).Error
}

// Update updates a ShoppingList in the database
func (s *ShoppingListWrapper) Update(data interface{}) error {
	shoppingList := data.(model.ShoppingList)

	return s.DB.Save(&shoppingList).Error
}

// Delete deletes a ShoppingList in the database
func (s *ShoppingListWrapper) Delete(data interface{}) error {
	shoppingList := data.(model.ShoppingList)

	return s.DB.Delete(&shoppingList).Error
}

// CreateShoppingListWrapper created the user wrapper using a given reference to gorm.DB
func CreateShoppingListWrapper(db *gorm.DB) *ShoppingListWrapper {
	return &ShoppingListWrapper{
		DB: db,
	}
}

// GetShoppingList returns the shopping list based on its id
func (s *ShoppingListWrapper) GetShoppingList(id uint) (model.ShoppingList, error) {
	var shoppingList model.ShoppingList

	err := s.DB.First(&shoppingList, id).Error
	if err != nil {
		return model.ShoppingList{}, err
	}

	return shoppingList, nil
}

// AddItem adds an Item to a given shopping list and returns the modified shopping list
func (s *ShoppingListWrapper) AddItem(item model.Item) (model.ShoppingList, error) {
	var shoppingList model.ShoppingList

	err := s.DB.First(&shoppingList, item.ShoppingListID).Error
	if err != nil {
		return model.ShoppingList{}, err
	}

	shoppingList.Items = append(shoppingList.Items, item)
	err = s.DB.Save(&shoppingList).Error
	if err != nil {
		return model.ShoppingList{}, err
	}

	s.DB.Preload("Items").Find(&shoppingList)

	return shoppingList, nil
}

// GetItems fetches the items of a given shopping list
func (s *ShoppingListWrapper) GetItems(shoppingListID uint) []model.Item {
	shoppingList, err := s.GetShoppingList(shoppingListID)
	if err != nil {
		return nil
	}
	s.DB.Preload("Items").Find(&shoppingList)
	return shoppingList.Items
}
