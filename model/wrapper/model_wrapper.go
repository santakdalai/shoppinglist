package wrapper

// ModelWrapper is an interface representing any database table
type ModelWrapper interface {
	Insert(data interface{}) error
	Update(data interface{}) error
}
