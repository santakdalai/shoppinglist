package model

import (
	"github.com/jinzhu/gorm"
)

// ShoppingList model
type ShoppingList struct {
	gorm.Model
	Title string `gorm:"not null"`
	// creation time is already present in gorm.Model
	Users []User `gorm:"many2many:user_shopping_lists;association_foreignkey:UserID"`
	Items []Item
}

// GetItems fetches the items of a given shopping list
func (s *ShoppingList) GetItems(db *gorm.DB) []Item {
	items := make([]Item, 0)
	db.Model(&s).Related(&s.Items)
	return items
}
