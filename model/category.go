package model

import (
	"github.com/jinzhu/gorm"
)

// Category model
type Category struct {
	gorm.Model
	Name        string `gorm:"not null"`
	Description string
	Items       []Item
}
