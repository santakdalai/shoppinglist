package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"net/http"
	"shoppinglist/cache"
	"shoppinglist/model"
)

// UserController deals with handling operations related to users
type UserController struct{}

// FetchShoppingLists fetches shopping lists of a given user
func (u *UserController) FetchShoppingLists(context *gin.Context) {
	status, resp := cache.AuthorizeSessionToken(context)
	if status != http.StatusOK {
		context.JSON(status, gin.H{
			"status":        status,
			"message":       "User unauthorised",
			"shopping_list": []model.ShoppingList{},
		})
		return
	}

	userID := string(resp.([]byte))
	shoppingLists := userWrapper.GetShoppingLists(userID)

	// hide passwords in response
	for i := 0; i < len(shoppingLists); i++ {
		for j := 0; j < len(shoppingLists[i].Users); j++ {
			shoppingLists[i].Users[j].Password = ""
		}
	}

	if len(shoppingLists) == 0 {
		log.Warn().Msgf("No shopping list found associated to user id: %s", userID)
		context.JSON(http.StatusNotFound, gin.H{
			"status":        http.StatusNotFound,
			"message":       "Shopping list not found for the user",
			"shopping_list": []model.ShoppingList{},
		})
		return
	}

	log.Info().Msgf("Shopping list found associated to user id: %s", userID)
	context.JSON(http.StatusOK, gin.H{
		"status":        http.StatusOK,
		"message":       "Shopping lists found for the user",
		"shopping_list": shoppingLists,
	})
}

// GetAllUsers fetches list of all present user IDs
func (u *UserController) GetAllUsers(context *gin.Context) {
	status, _ := cache.AuthorizeSessionToken(context)
	if status != http.StatusOK {
		context.JSON(status, gin.H{
			"status":        status,
			"message":       "User unauthorised",
			"user_ids": []string{},
		})
		return
	}

	userIDs := userWrapper.GetAllUsers()
	if len(userIDs) == 0 {
		log.Warn().Msgf("No user found in the DB")
		context.JSON(http.StatusNotFound, gin.H{
			"status":        http.StatusNotFound,
			"message":       "No user found",
			"user_ids": userIDs,
		})
		return
	}

	log.Info().Msgf("Successfully retrieved users present in the database")
	context.JSON(http.StatusOK, gin.H{
		"status":        http.StatusOK,
		"message":       "All users retrieved",
		"user_ids": userIDs,
	})
}