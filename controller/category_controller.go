package controller

import (
	"net/http"
	"shoppinglist/cache"
	"shoppinglist/model"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
)

// CategoryController is the controller responsible for handling categories
type CategoryController struct{}

// CreateCategory creates a new category for items
func (c *CategoryController) CreateCategory(context *gin.Context) {
	status, _ := cache.AuthorizeSessionToken(context)
	if status != http.StatusOK {
		context.JSON(status, gin.H{
			"status":      status,
			"message":     "User unauthorised",
			"category_id": 0,
		})
		return
	}

	inputCategory := struct {
		Name        string `json:"name"`
		Description string `json:"description"`
	}{}

	if err := context.BindJSON(&inputCategory); err != nil {
		log.Error().Msgf("Error occurred while reading the input request body. %s", err.Error())

		context.JSON(http.StatusBadRequest, gin.H{
			"status":      http.StatusBadRequest,
			"message":     "Error in the request body",
			"category_id": 0,
		})
		return
	}

	var newCategory model.Category
	newCategory.Name = inputCategory.Name
	newCategory.Description = inputCategory.Description

	err := categoryWrapper.Insert(&newCategory)

	if err != nil {
		log.Error().Msgf(
			"Error occurred while creating category with name: %s, error: %s",
			newCategory.Name,
			err.Error())
		context.JSON(http.StatusInternalServerError, gin.H{
			"status":      http.StatusInternalServerError,
			"message":     "Internal Server Error",
			"category_id": 0,
		})
		return
	}

	if newCategory.ID == 0 {
		log.Error().Msgf("Error occurred while creating category with name: %s", newCategory.Name)
		context.JSON(http.StatusInternalServerError, gin.H{
			"status":      http.StatusInternalServerError,
			"message":     "Internal Server Error",
			"category_id": 0,
		})
		return
	}

	log.Info().Msgf("Category with name: %s successfully created", newCategory.Name)
	context.JSON(http.StatusCreated, gin.H{
		"status":      http.StatusCreated,
		"message":     "Category successfully created",
		"category_id": newCategory.ID,
	})

}
