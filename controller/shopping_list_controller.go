package controller

import (
	"net/http"
	"shoppinglist/cache"
	"shoppinglist/model"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
)

// ShoppingListController is the controller responsible for handling shopping lists
type ShoppingListController struct{}

// CreateShoppingList creates shopping list for the user in the context
func (s *ShoppingListController) CreateShoppingList(context *gin.Context) {
	status, resp := cache.AuthorizeSessionToken(context)
	if status != http.StatusOK {
		context.JSON(status, gin.H{
			"status":           status,
			"message":          "User unauthorised",
			"shopping_list_id": 0,
		})
		return
	}

	inputShoppingList := struct {
		Title string `json:"title"`
	}{}

	if err := context.BindJSON(&inputShoppingList); err != nil {
		log.Error().Msgf("Error occurred while reading the input request body. %s", err.Error())
		context.JSON(http.StatusBadRequest, gin.H{
			"status":           http.StatusBadRequest,
			"message":          "Error in the request body",
			"shopping_list_id": 0,
		})
		return
	}

	var newShoppingList model.ShoppingList
	newShoppingList.Title = inputShoppingList.Title

	// get user from the DB
	user, _ := userWrapper.GetUser(string(resp.([]byte)))
	newShoppingList.Users = make([]model.User, 0)
	newShoppingList.Users = append(newShoppingList.Users, user)

	// insert shopping list to DB
	err := shoppingListWrapper.Insert(&newShoppingList)

	if err != nil {
		log.Error().Msgf(
			"Error occurred while creating the shopping list for user id: %s with title: %s, error: %s",
			user.UserID,
			newShoppingList.Title,
			err.Error())
		context.JSON(http.StatusInternalServerError, gin.H{
			"status":           http.StatusInternalServerError,
			"message":          "Internal Server Error",
			"shopping_list_id": 0,
		})
		return
	}

	if newShoppingList.ID == 0 {
		log.Error().Msgf(
			"Unknown Error occurred while creating the shopping list for user id: %s with title: %s",
			user.UserID,
			newShoppingList.Title)
		context.JSON(http.StatusInternalServerError, gin.H{
			"status":           http.StatusInternalServerError,
			"message":          "Internal Server Error",
			"shopping_list_id": 0,
		})
		return
	}

	log.Info().Msgf(
		"Shopping list successfully created for user: %s with title: %s",
		user.UserID, newShoppingList.Title)
	context.JSON(http.StatusCreated, gin.H{
		"status":           http.StatusCreated,
		"message":          "Shopping list successfully created",
		"shopping_list_id": newShoppingList.ID,
	})

}

// DeleteShoppingList deletes a shopping list based on the ID given in the context
func (s *ShoppingListController) DeleteShoppingList(context *gin.Context) {
	status, _ := cache.AuthorizeSessionToken(context)
	if status != http.StatusOK {
		context.JSON(status, gin.H{
			"status":  status,
			"message": "User unauthorised",
		})
		return
	}

	inputDeleteShoppingList := struct {
		ShoppingListID uint `json:"shopping_list_id"`
	}{}
	if err := context.BindJSON(&inputDeleteShoppingList); err != nil {
		log.Error().Msgf("Error occurred while reading the input request body. %s", err.Error())
		context.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"message": "Error in the request body",
		})
		return
	}

	shoppingList, err := shoppingListWrapper.GetShoppingList(inputDeleteShoppingList.ShoppingListID)
	if err != nil {
		log.Error().Msgf(
			"Error occurred while fetching the shopping list id: %d. Error: %s",
			inputDeleteShoppingList.ShoppingListID,
			err.Error())
		context.JSON(http.StatusInternalServerError, gin.H{
			"status":  http.StatusInternalServerError,
			"message": "Internal Server Error",
		})
		return
	}

	err = shoppingListWrapper.Delete(shoppingList)

	if err != nil {
		log.Error().Msgf(
			"Error occurred while deleting the shopping list id: %d with title: %s. Error: %s",
			shoppingList.ID,
			shoppingList.Title,
			err.Error())
		context.JSON(http.StatusInternalServerError, gin.H{
			"status":  http.StatusInternalServerError,
			"message": "Internal Server Error",
		})
		return
	}

	log.Info().Msgf("Shopping list id: %d successfully deleted", shoppingList.ID)
	context.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"message": "Shopping list successfully deleted",
	})
}

// ShareShoppingList shares a shopping list to a mentioned user in the context
func (s *ShoppingListController) ShareShoppingList(context *gin.Context) {
	status, _ := cache.AuthorizeSessionToken(context)
	if status != http.StatusOK {
		context.JSON(status, gin.H{
			"status":  status,
			"message": "User unauthorised",
		})
		return
	}

	inputSharingInfo := struct {
		ShoppingListID uint     `json:"shopping_list_id"`
		ShareToUserIDs []string `json:"share_to_users"`
	}{}

	if err := context.BindJSON(&inputSharingInfo); err != nil {
		log.Error().Msgf("Error occurred while reading the input request body. %s", err.Error())

		context.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"message": "Error in the request body",
		})
		return
	}

	shoppingList, _ := shoppingListWrapper.GetShoppingList(inputSharingInfo.ShoppingListID)
	for _, userID := range inputSharingInfo.ShareToUserIDs {
		user, _ := userWrapper.GetUser(userID)
		shoppingList.Users = append(shoppingList.Users, user)
	}

	err := shoppingListWrapper.Update(shoppingList)

	if err != nil {
		log.Error().Msgf(
			"Error occurred while sharing the shopping list id: %d with title: %s, users: %v. Error: %s",
			shoppingList.ID,
			shoppingList.Title,
			inputSharingInfo.ShareToUserIDs,
			err.Error())
		context.JSON(http.StatusInternalServerError, gin.H{
			"status":  http.StatusInternalServerError,
			"message": "Internal Server Error",
		})
		return
	}

	log.Info().Msgf(
		"Shopping list id: %d successfully shared with users: %v ",
		shoppingList.ID, inputSharingInfo.ShareToUserIDs)
	context.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"message": "Shopping list successfully shared",
	})
}

// AddItem adds an item to a given shopping list
func (s *ShoppingListController) AddItem(context *gin.Context) {
	status, _ := cache.AuthorizeSessionToken(context)
	if status != http.StatusOK {
		context.JSON(status, gin.H{
			"status":        status,
			"message":       "User unauthorised",
			"shopping_list": nil,
		})
		return
	}

	inputItemDetails := struct {
		Name           string `json:"name"`
		Description    string `json:"description"`
		ShoppingListID uint   `json:"shopping_list_id"`
		CategoryID     uint   `json:"category_id"`
	}{}

	if err := context.BindJSON(&inputItemDetails); err != nil {
		log.Error().Msgf("Error occurred while reading the input request body. %s", err.Error())
		context.JSON(http.StatusBadRequest, gin.H{
			"status":        http.StatusBadRequest,
			"message":       "Error in the request body",
			"shopping_list": nil,
		})
		return
	}

	var item model.Item
	item.Name = inputItemDetails.Name
	item.Description = inputItemDetails.Description
	item.ShoppingListID = inputItemDetails.ShoppingListID
	item.CategoryID = inputItemDetails.CategoryID

	// validate category
	_, err := categoryWrapper.GetCategory(item.CategoryID)
	if err != nil {
		log.Error().Msgf(
			"Error occurred while validating category id: %d for item: %s. %s",
			item.CategoryID,
			item.Name,
			err.Error())
		context.JSON(http.StatusInternalServerError, gin.H{
			"status":        http.StatusInternalServerError,
			"message":       "Error occurred while validating category for the item",
			"shopping_list": nil,
		})
		return
	}

	shoppingList, err := shoppingListWrapper.AddItem(item)
	if err != nil {
		log.Error().Msgf(
			"Error occurred while adding item name: %s, to the shopping list id: %d. %s",
			item.Name,
			item.ShoppingListID,
			err.Error())
		context.JSON(http.StatusInternalServerError, gin.H{
			"status":        http.StatusInternalServerError,
			"message":       "Error occurred while adding item to the shopping list",
			"shopping_list": nil,
		})
		return
	}

	log.Info().Msgf(
		"Successfully added item %s to the shopping list %s",
		item.Name,
		shoppingList.Title)
	context.JSON(http.StatusOK, gin.H{
		"status":        http.StatusOK,
		"message":       "Successfully added the item",
		"shopping_list": shoppingList,
	})

}

// GetItems fetches list of Items of a given shopping list
func (s *ShoppingListController) GetItems(context *gin.Context) {
	status, _ := cache.AuthorizeSessionToken(context)
	if status != http.StatusOK {
		context.JSON(status, gin.H{
			"status":  status,
			"message": "User unauthorised",
			"items":   []model.Item{},
		})
		return
	}

	shoppingListID, err := strconv.Atoi(context.Request.Header.Get("shopping_list_id"))

	if err != nil {
		log.Error().Msgf("Error occurred while reading the input request header. %s", err.Error())
		context.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"message": "Error in the request header",
			"items":   []model.Item{},
		})
		return
	}

	items := shoppingListWrapper.GetItems(uint(shoppingListID))

	if len(items) == 0 {
		log.Warn().Msgf("No items found associated to shopping list id: %d", shoppingListID)
		context.JSON(http.StatusNotFound, gin.H{
			"status":  http.StatusNotFound,
			"message": "No items found for the shopping list",
			"items":   []model.Item{},
		})
		return
	}

	log.Info().Msgf("Items found for shopping list id: %d", shoppingListID)
	context.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"message": "Items found for the shopping list",
		"items":   items,
	})
}
