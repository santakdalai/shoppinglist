package controller

import (
	"net/http"
	"shoppinglist/cache"
	"shoppinglist/model"
	"shoppinglist/util"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
)

// SignupController is the controller responsible for handling login and signup for users
type SignupController struct{}

// AddUser adds a new user to the database while signing up
func (s *SignupController) AddUser(context *gin.Context) {
	inputUser := struct {
		UserID   string `json:"user_id"`
		Name     string `json:"name"`
		Email    string `json:"email"`
		Password string `json:"password"`
	}{}

	if err := context.BindJSON(&inputUser); err != nil {
		log.Error().Msgf("Error occurred while reading the input request body. %s", err.Error())
		context.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"message": "Error in the request body",
		})
		return
	}

	var newUser model.User
	newUser.UserID = inputUser.UserID
	newUser.Email = inputUser.Email
	newUser.Name = inputUser.Name

	//hash the password using bcrypt
	encryptedPassword, err := util.HashPassword(inputUser.Password)
	newUser.Password = encryptedPassword
	if err != nil {
		log.Error().Msgf("Error occurred while hashing the password")
		context.JSON(http.StatusInternalServerError, gin.H{
			"status":  http.StatusInternalServerError,
			"message": "Internal Server Error",
		})
		return
	}

	if userWrapper.UserAlreadyExists(newUser.UserID) {
		log.Error().Msgf("Error occurred while creating the user for user id: %s, error: %s", newUser.UserID, "User already exists")
		context.JSON(http.StatusConflict, gin.H{
			"status":  http.StatusConflict,
			"message": "User already exists",
		})
		return
	}

	err = userWrapper.Insert(&newUser)

	if err != nil {
		log.Error().Msgf("Error occurred while creating the user for user id: %s, error: %s", newUser.UserID, err.Error())
		context.JSON(http.StatusInternalServerError, gin.H{
			"status":  http.StatusInternalServerError,
			"message": "Internal Server Error",
		})
		return
	}

	if newUser.ID == 0 {
		log.Error().Msgf("Error occurred while creating the user for user id: %s", newUser.UserID)
		context.JSON(http.StatusInternalServerError, gin.H{
			"status":  http.StatusInternalServerError,
			"message": "Internal Server Error",
		})
		return
	}

	log.Info().Msgf("User successfully created with User ID: %d, for userID: %s", newUser.ID, newUser.UserID)
	context.JSON(http.StatusCreated, gin.H{
		"status":  http.StatusCreated,
		"message": "User successfully created",
	})
}

// Login logs in a user to the application based on the credentials passed in the context
func (s *SignupController) Login(context *gin.Context) {
	loginInfo := struct {
		UserID   string `json:"user_id"`
		Password string `json:"password"`
	}{}

	if err := context.BindJSON(&loginInfo); err != nil {
		log.Error().Msgf("Error occurred while reading the input request body. %s", err.Error())
		context.JSON(http.StatusBadRequest, gin.H{
			"status":        http.StatusBadRequest,
			"message":       "Error in the request body",
			"session_token": nil,
		})
		return
	}

	user, err := userWrapper.GetUser(loginInfo.UserID)

	if err != nil {
		log.Error().Msgf("Error occurred while searching for the user %v in the database. %s", loginInfo.UserID, err.Error())
		context.JSON(http.StatusNotFound, gin.H{
			"status":        http.StatusNotFound,
			"message":       "Mentioned user not found",
			"session_token": nil,
		})
		return
	}

	if !util.CheckPasswordHash(loginInfo.Password, user.Password) {
		log.Error().Msgf("Invalid username/email or password for user: %s", loginInfo.UserID)
		context.JSON(http.StatusUnauthorized, gin.H{
			"status":        http.StatusUnauthorized,
			"message":       "Invalid username/email or password",
			"session_token": nil,
		})
		return
	}

	// Create a random session token
	sessionToken, err := cache.CreateSessionToken(user)
	if err != nil {
		log.Error().Msg(err.Error())
		context.JSON(http.StatusInternalServerError, gin.H{
			"status":        http.StatusInternalServerError,
			"message":       "Internal Server Error",
			"session_token": nil,
		})
		return
	}

	log.Info().Msgf("User: %s, successfully logged in", loginInfo.UserID)
	context.JSON(http.StatusOK, gin.H{
		"status":        http.StatusOK,
		"message":       "User successfully logged in",
		"session_token": sessionToken,
	})
}

// Logout performs log out based on the session cookie passed in the context
func (s *SignupController) Logout(context *gin.Context) {
	status, resp := cache.AuthorizeSessionToken(context)
	if status != http.StatusOK {
		context.JSON(status, gin.H{
			"status":  status,
			"message": "User unauthorised",
		})
		return
	}

	if !cache.DeleteSessionToken(context) {
		context.JSON(status, gin.H{
			"status":  http.StatusInternalServerError,
			"message": "Internal error while logging out",
		})
		return
	}

	log.Info().Msgf("User successfully logged out userID: %s", string(resp.([]byte)))
	context.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"message": "User successfully logged out",
	})
}
