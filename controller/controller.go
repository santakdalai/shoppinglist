package controller

import (
	"shoppinglist/cache"
	"shoppinglist/model/wrapper"

	"github.com/jinzhu/gorm"
)

var (
	db                  *gorm.DB
	userWrapper         *wrapper.UserWrapper
	shoppingListWrapper *wrapper.ShoppingListWrapper
	categoryWrapper     *wrapper.CategoryWrapper
	itemWrapper         *wrapper.ItemWrapper
)

// InitializeController initializes the controllers with the gorm DB, as provided
func InitializeController(DB *gorm.DB) {
	db = DB
	userWrapper = wrapper.CreateUserWrapper(db)
	shoppingListWrapper = wrapper.CreateShoppingListWrapper(db)
	categoryWrapper = wrapper.CreateCategoryWrapper(db)
	itemWrapper = wrapper.CreateItemWrapper(db)

	cache.InitCache()
}
