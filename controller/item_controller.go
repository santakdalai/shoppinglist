package controller

import (
	"net/http"
	"shoppinglist/cache"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
)

// ItemController is the controller responsible for handling items
type ItemController struct{}

// MarkItemBought marks an item as bought bya given user
func (i *ItemController) MarkItemBought(context *gin.Context) {
	status, resp := cache.AuthorizeSessionToken(context)
	if status != http.StatusOK {
		context.JSON(status, gin.H{
			"status":  status,
			"message": "User unauthorised",
			"item":    nil,
		})
		return
	}

	inputMarkItemDetails := struct {
		ItemID uint `json:"item_id"`
	}{}

	if err := context.BindJSON(&inputMarkItemDetails); err != nil {
		log.Error().Msgf("Error occurred while reading the input request body. %s", err.Error())
		context.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"message": "Error in the request body",
			"item":    nil,
		})
		return
	}

	boughtBy := string(resp.([]byte))

	item, err := itemWrapper.MarkBought(inputMarkItemDetails.ItemID, boughtBy)
	if err != nil {
		log.Error().Msgf(
			"Error occurred while marking item id: %d bought by user id: %s. Error: %s",
			inputMarkItemDetails.ItemID,
			boughtBy,
			err.Error())
		context.JSON(http.StatusInternalServerError, gin.H{
			"status":  http.StatusInternalServerError,
			"message": "Error occurred while adding item to the shopping list",
			"item":    nil,
		})
		return
	}

	log.Info().Msgf(
		"Successfully marked item id: %d bought by user id: %s",
		item.ID,
		item.BoughtBy)
	context.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"message": "Successfully added the item",
		"item":    item,
	})

}

// DeleteItem deletes an Item based on the ID given in the context
func (i *ItemController) DeleteItem(context *gin.Context) {
	status, _ := cache.AuthorizeSessionToken(context)
	if status != http.StatusOK {
		context.JSON(status, gin.H{
			"status":  status,
			"message": "User unauthorised",
		})
		return
	}

	inputDeleteItem := struct {
		ItemID uint `json:"item_id"`
	}{}
	if err := context.BindJSON(&inputDeleteItem); err != nil {
		log.Error().Msgf("Error occurred while reading the input request body. %s", err.Error())
		context.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"message": "Error in the request body",
		})
		return
	}

	item, err := itemWrapper.GetItem(inputDeleteItem.ItemID)
	if err != nil {
		log.Error().Msgf(
			"Error occurred while fetching the item id: %d. Error: %s",
			inputDeleteItem.ItemID,
			err.Error())
		context.JSON(http.StatusInternalServerError, gin.H{
			"status":  http.StatusInternalServerError,
			"message": "Internal Server Error",
		})
		return
	}

	err = itemWrapper.Delete(item)

	if err != nil {
		log.Error().Msgf(
			"Error occurred while deleting the item id: %d with name: %s. Error: %s",
			item.ID,
			item.Name,
			err.Error())
		context.JSON(http.StatusInternalServerError, gin.H{
			"status":  http.StatusInternalServerError,
			"message": "Internal Server Error",
		})
		return
	}

	log.Info().Msgf("Item id: %d successfully deleted", item.ID)
	context.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"message": "Item successfully deleted",
	})
}
