package model

import (
	"database/sql"
	"regexp"
	"shoppinglist/model"
	"shoppinglist/model/wrapper"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
)

// ShoppingListTestSuite is test suite for user model
type ShoppingListTestSuite struct {
	suite.Suite
	DB   *gorm.DB
	mock sqlmock.Sqlmock

	modelWrapper wrapper.ModelWrapper
	shoppingList *model.ShoppingList
}

// SetupSuite initializes the ShoppingListTestSuite with required mocks
func (s *ShoppingListTestSuite) SetupSuite() {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	s.DB, err = gorm.Open("mysql", db)
	require.NoError(s.T(), err)

	s.DB.LogMode(true)

	s.modelWrapper = wrapper.CreateShoppingListWrapper(s.DB)
}

// AfterTest checks whether expectations of sql-mock were met
func (s *ShoppingListTestSuite) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestShoppingListInit(t *testing.T) {
	suite.Run(t, new(ShoppingListTestSuite))
}

// TestShoppingListInsert tests the Insert method of the User model
func (s *ShoppingListTestSuite) TestShoppingListInsert() {

	shoppingList := model.ShoppingList{
		Title: "testTitle",
	}

	shoppingListInsertSQL := regexp.QuoteMeta(
		"INSERT INTO `shopping_lists` (`created_at`,`updated_at`,`deleted_at`,`title`) VALUES (?,?,?,?)")

	s.mock.ExpectBegin()
	s.mock.ExpectExec(shoppingListInsertSQL).
		WithArgs(sqlmock.AnyArg(), sqlmock.AnyArg(), sqlmock.AnyArg(), shoppingList.Title).
		WillReturnResult(sqlmock.NewResult(1, 1)) // new row inserted would always have ID 1

	s.mock.ExpectCommit()

	err := s.modelWrapper.Insert(&shoppingList)

	require.NoError(s.T(), err)
}
