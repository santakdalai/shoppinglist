package model

import (
	"database/sql"
	"regexp"
	"shoppinglist/model"
	"shoppinglist/model/wrapper"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
)

// CategoryTestSuite is test suite for Category model
type CategoryTestSuite struct {
	suite.Suite
	DB   *gorm.DB
	mock sqlmock.Sqlmock

	modelWrapper wrapper.ModelWrapper
	category     *model.Category
}

// SetupSuite initializes the CategoryTestSuite with required mocks
func (s *CategoryTestSuite) SetupSuite() {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	s.DB, err = gorm.Open("mysql", db)
	require.NoError(s.T(), err)

	s.DB.LogMode(true)

	s.modelWrapper = wrapper.CreateCategoryWrapper(s.DB)
}

// AfterTest checks whether expectations of sql-mock were met
func (s *CategoryTestSuite) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestCategoryInit(t *testing.T) {
	suite.Run(t, new(CategoryTestSuite))
}

// TestUserInsert tests the Insert method of the User model
func (s *CategoryTestSuite) TestCategoryInsert() {
	category := model.Category{
		Name:        "TestCategoryName",
		Description: "Test category description",
	}

	categoryInsertSQL := regexp.QuoteMeta(
		"INSERT INTO `categories` (`created_at`,`updated_at`,`deleted_at`,`name`,`description`) VALUES (?,?,?,?,?)")

	s.mock.ExpectBegin()
	s.mock.ExpectExec(categoryInsertSQL).
		WithArgs(sqlmock.AnyArg(), sqlmock.AnyArg(), sqlmock.AnyArg(), category.Name, category.Description).
		WillReturnResult(sqlmock.NewResult(1, 1)) // new row inserted would always have ID 1

	s.mock.ExpectCommit()

	err := s.modelWrapper.Insert(&category)

	require.NoError(s.T(), err)
}
