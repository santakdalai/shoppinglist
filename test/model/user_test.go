package model

import (
	"database/sql"
	"regexp"
	"shoppinglist/model"
	"shoppinglist/model/wrapper"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
)

// UserTestSuite is test suite for user model
type UserTestSuite struct {
	suite.Suite
	DB   *gorm.DB
	mock sqlmock.Sqlmock

	modelWrapper wrapper.ModelWrapper
	user         *model.User
}

// SetupSuite initializes the UserTestSuite with required mocks
func (s *UserTestSuite) SetupSuite() {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	s.DB, err = gorm.Open("mysql", db)
	require.NoError(s.T(), err)

	s.DB.LogMode(true)

	s.modelWrapper = wrapper.CreateUserWrapper(s.DB)
}

// AfterTest checks whether expectations of sql-mock were met
func (s *UserTestSuite) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestUserInit(t *testing.T) {
	suite.Run(t, new(UserTestSuite))
}

// TestUserInsert tests the Insert method of the User model
func (s *UserTestSuite) TestUserInsert() {
	user := model.User{
		UserID:   "testUser",
		Name:     "testUserName",
		Email:    "test@abc.com",
		Password: "alpha@123",
	}

	userInsertSQL := regexp.QuoteMeta(
		"INSERT INTO `users` (`created_at`,`updated_at`,`deleted_at`,`user_id`,`name`,`email`,`password`) VALUES (?,?,?,?,?,?,?)")

	s.mock.ExpectBegin()
	s.mock.ExpectExec(userInsertSQL).
		WithArgs(sqlmock.AnyArg(), sqlmock.AnyArg(), sqlmock.AnyArg(), user.UserID, user.Name, user.Email, user.Password).
		WillReturnResult(sqlmock.NewResult(1, 1)) // new row inserted would always have ID 1

	s.mock.ExpectCommit()

	err := s.modelWrapper.Insert(&user)

	require.NoError(s.T(), err)
}
