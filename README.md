# Shopping List Server

Shopping list server is typical MVC based server to facilitate with the functionalities of a shopping list.


## How to run

This code uses docker-compose to run. Please make sure that you have docker-compose [installed](https://docs.docker.com/compose/install/).

### Starting the server 
To start the server you an run this command
```bash
docker-compose up -d
```
You can run this command to verify the required containers are up and running
```bash
docker-compose ps
```
You should see output similar to this
```
           Name                          Command               State           Ports         
---------------------------------------------------------------------------------------------
shoppinglist_mysql_1          docker-entrypoint.sh mysqld      Up      3306/tcp, 33060/tcp   
shoppinglist_redis_1          docker-entrypoint.sh redis ...   Up      6379/tcp              
shoppinglist_shoppinglist_1   ./shopping_list                  Up      0.0.0.0:9797->9797/tcp

```
### Stopping the server
This server could be stopped by stopped by this command
```
docker-compose down
```

If you additionally want to cleanup the volume then you must use this command
```
docker-compose down -v
```

### Starting server after changes to the code
If you have made any changes to the code than you must update the container. You can do so by following command
```
docker-compose build
```
Then you can run `docker-compose up -d` to run the server, as mentioned earlier.

## API Details
 * GET Ping

	```
	http://localhost:9797/ping
	```
---

 * POST User Signup
	```
	http://localhost:9797/user/signup
	```
	BODY raw

	---
	```javascript
	{
		"user_id": "sumithra.k",
		"name": "Sumithra Kashyap",
		"email": "sumithra.k@abc.com",
		"password": "sumithra@123"
	}
	```
---
* POST User Login
	```
	http://localhost:9797/user/login
	```
	HEADERS

	----------

	Content-Type - **application/json**
	
	BODY raw

	----------

	```javascript
	{
		"user_id": "sumithra.k",
		"password": "sumithra@123"
	}
	```
---
*  POST User Logout
	```
	http://localhost:9797/user/logout
	```
	HEADERS
	
	---
	session_token - **6e28c633-3fd1-4bf6-b57d-77652e2fd51d**
---

* GET Fetch Shopping List for a User
	```
	http://localhost:9797/user/fetch-shopping-list
	```
	HEADERS

	---
	session_token - **51251644-d2d3-4756-8e59-a6c73f2a9b69**
---

* POST Create a Shopping List
	```
	http://localhost:9797/shopping-list/create
	```
	HEADERS
	
	---
	session_token - **95964367-9870-4709-99a0-cc0bca6711b6**
	Content-Type - application/json
	
	BODY raw

	----------

	```javascript
	{
		"title": "Sarees"
	}
	```
---
* POST Share a Shopping List with a List of Users
	```
	http://localhost:9797/shopping-list/share
	```

	HEADERS

	----------

	session_token - **51251644-d2d3-4756-8e59-a6c73f2a9b69**

	Content-Type - **application/json**

	BODY raw

	----------

	```javascript
	{
		"shopping_list_id": 3,
		"share_to_users": [
			"prakash.m",
			"tango.c"
			]
	}
	```
---
* POST Delete a Shopping List
	```
	http://localhost:9797/shopping-list/delete
	```
	HEADERS

	----------

	session_token - **51251644-d2d3-4756-8e59-a6c73f2a9b69**

	Content-Type - **application/json**

	BODY raw

	----------

	```javascript
	{
		"shopping_list_id": 7
	}
	```
---
* POST Create a Category for Items
	```
	http://localhost:9797/category/create
	```
	HEADERS

	----------

	session_token - **51251644-d2d3-4756-8e59-a6c73f2a9b69**

	Content-Type - **application/json**

	BODY raw

	----------

	```javascript
	{
		"name": "Adult",
		"description": "Items suitable for adults"
	}
	```
---
* POST Add an Item to a Shopping List
	```
	http://localhost:9797/shopping-list/add-item
	```
	HEADERS

	----------

	session_token - **51251644-d2d3-4756-8e59-a6c73f2a9b69**

	Content-Type - **application/json**

	BODY raw

	----------

	```javascript
	{
		"name": "yamaha",
		"description": "yama rx100",
		"shopping_list_id": 8,
		"category_id": 3
	}
	```
---
* POST Mark an Item as bought
	```
	http://localhost:9797/item/mark
	```
	HEADERS

	----------

	Content-Type - **application/json**

	session_token - **51251644-d2d3-4756-8e59-a6c73f2a9b69**

	BODY raw

	----------

	```javascript
	{
		"item_id": 12
	}
	```
---
* POST Delete an Item
	```
	http://localhost:9797/item/delete
	```
	HEADERS

	----------

	session_token - **51251644-d2d3-4756-8e59-a6c73f2a9b69**

	Content-Type - **application/json**

	BODY raw

	----------

	```javascript
	{
		"item_id": 7
	}
	```
---
* GET Get Items for a Shopping List
	```
	http://localhost:9797/shopping-list/items
	```
	HEADERS

	---
	session_token - **51251644-d2d3-4756-8e59-a6c73f2a9b69**

	shopping_list_id - **3**
---
* GET Get All User IDs in the system
	```
	http://localhost:9797/user/get-all-user-ids
	```
	HEADERS

	----------

	session_token - **51251644-d2d3-4756-8e59-a6c73f2a9b69**