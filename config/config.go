package config

import (
	"fmt"

	"github.com/spf13/viper"
)

// LoadConfig reads the config file and initializes viper
func LoadConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("toml")
	viper.AddConfigPath("./config")

	err := viper.ReadInConfig()
	if err != nil {
		// Handle errors reading the config file
		panic(fmt.Errorf("fatal error config file: %s", err))
	}
}
